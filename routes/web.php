<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//CRUD Content
Route::resource('content', 'ContentController');
Route::resource('jeniscontent', 'JenisContentController');
Route::resource('obat', 'obatController');
Route::resource('jenisObat', 'jenisObatController');


Route::get('/login', 'AuthController@index');
Route::post('/login', 'AuthController@store');
Route::get('/logout', 'AuthController@logout');
Route::get('/register', 'AuthorizeController@index');
Route::post('/register', 'AuthorizeController@store');

//middleware, redirect berdasarkan status si user
Route::group(['middleware' => ['auth']], function () {
    //middleware auth_check and redirect to Dashboard Admin
    Route::group(['middleware' => ['auth_check:admin']], function () {
        Route::resource('/dashboard_admin', 'AdminController');
        Route::resource('/kelola_user', 'UserController');
        Route::resource('/dokter', 'DokterController');
    });

    //middleware auth_check and redirect to Dashboard Staff
    Route::group(['middleware' => ['auth_check:staff']], function () {
        Route::resource('/dashboard_staff', 'StaffController');
    });
});
