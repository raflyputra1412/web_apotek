<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateObatTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('obat', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nama_obat');
            $table->string('gambar');
            $table->string('ketersediaan');
            $table->string('indikasi');
            $table->integer('harga');
            
            // foreign key references jenis obat id
            $table->unsignedBigInteger('jenis_obat_id');
            $table->foreign('jenis_obat_id')
                    ->references('id')->on('jenis_obat')
                    ->onDelete('cascade');
            
            // foreign key references users id
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')
                    ->references('id')->on('users')
                    ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('obat');
    }
}
