<?php

use Illuminate\Database\Seeder;
use App\User;


class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        DB::table('users')->insert([
            "nama_user" => "admin",
            "password" => Hash::make('12345'),
            "alamat" => "Denpasar, bali",         
            "phone" => 123456789,   
            "email" => "admin@gmail.com",
            "status" => "admin",
        ]);

        DB::table('users')->insert([
            "nama_user" => "staff",
            "password" => Hash::make('12345'),
            "alamat" => "Denpasar, bali",         
            "phone" => 123456789,   
            "email" => "staff@gmail.com",
            "status" => "staff",
        ]);
    }
}
