@extends('auth_layouts.app')


@section('content')

<form action="/register" method="POST">
    @csrf

    <div class="container contact">
        <div class="row">
            <div class="col-md-3">
                <div class="contact-info">
                    <h2 class="text-white">Form Registration</h2>
                </div>
            </div>
            <div class="col-md-9">
                <div class="contact-form">
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="nama_user">Nama:</label>
                        <div class="col-sm-10">          
                            <input type="text" class="form-control" id="nama_user" placeholder="Enter Name" name="nama_user">
                        </div>
				</div>
				<div class="form-group">
                    <label class="control-label col-sm-2" for="password">Password:</label>
                    <div class="col-sm-10">          
                        <input type="password" class="form-control" id="password" placeholder="Enter Password" name="password">
                    </div>
				</div>
				<div class="form-group">
                    <label class="control-label col-sm-2" for="alamat">Alamat:</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="alamat" placeholder="Enter Alamat" name="alamat">
                    </div>
				</div>
				<div class="form-group">
                    <label class="control-label col-sm-2" for="phone">Phone:</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="phone" placeholder="Enter Phone" name="phone">
                    </div>
				</div>
				<div class="form-group">
                    <label class="control-label col-sm-2" for="email">Email:</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="email" placeholder="Enter Email" name="email">
                    </div>
				</div>
                <input type="text" class="form-control" id="status" name="status" value="staff" hidden>
				</div>
				<div class="form-group">        
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-default">Register</button>
                    </div>
				</div>
			</div>
		</div>
	</div>
</div>
</form>

@endsection