@extends('admin.layout.master')
@section('judul')
  Halaman Jenis Obat
@endsection
@section('content')
<form action="/jenisObat/{{$jenisObat->id}}" method="POST">
  @csrf
  @method('PUT')
  <div class="form-group">
    <label >Nama Jenis Obat</label>
    <input type="text" name="nama" value="{{old('nama',$jenisObat->nama_jenis)}}" class="form-control" >
  </div>
  @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
  
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection