@extends('admin.layout.master')
@section('judul')
    Jenis Obat
@endsection
@push('scripts')
    <script src="{{asset('template/plugins/datatables/jquery.dataTables.js')}}"></script>
    <script src="{{asset('template/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
    <script>
    $(function () {
        $("#dataObat").DataTable();
    });
    </script>
@endpush
@push('style')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.12.1/datatables.min.css"/>

@endpush
@section('content')
<a href="/jenisObat/create" class="btn btn-primary btn-sm my-3">Input Jenis Obat Baru</a>
<table id="dataObat" class="table table-bordered table-striped">
    <thead>
    <tr>
      <th>No</th>
      <th>Nama Jenis Obat</th>
      <th>Action</th>
    </tr>
    </thead>
    <tbody>
        @forelse ($jenisObat as $key =>$item)
        <tr>
            <td>{{$key+1}}</td>
            <td>{{$item->nama_jenis}}</td>
            <td>
                <form action="/jenisObat/{{$item->id}}" method="POST">
                    @csrf
                    @method('DELETE')
                        <a href="/jenisObat/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit Obat</a>
                        <input type="submit" value="delete" class="btn btn-danger btn-sm">
                  </form>
            </td>
            </tr>
        @empty
            <tr>
                <td>Tidak ada data Obat</td>
            </tr>
        @endforelse
        
   </tbody>
</table>
@endsection
