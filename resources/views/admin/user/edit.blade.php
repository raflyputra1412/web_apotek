@extends('admin.layout.master')
@section('judul')
    Edit User
@endsection
@section('content')

<form action="/kelola_user/{{ $user->id }}" method="POST">
    @csrf
    @method('PUT')

    <div class="form-group">
        <label for="nama_user">Nama User: </label>
        <input type="text" name="nama_user" class="form-control" value="{{ old('nama_user', $user->nama_user) }}">
      @error('nama_user')
          <div class="alert alert-danger">{{ $message }}</div>
      @enderror
      </div>
  
      <div class="form-group">
          <label for="alamat">Alamat: </label>
          <input type="text" name="alamat" class="form-control" value="{{ old('alamat', $user->alamat) }}">
      @error('alamat')
          <div class="alert alert-danger">{{ $message }}</div>
      @enderror
        </div>
        
        <div class="form-group">
          <label for="phone">Phone: </label>
          <input type="text" name="phone" class="form-control" value="{{ old('phone', $user->phone) }}">
      @error('phone')
          <div class="alert alert-danger">{{ $message }}</div>
      @enderror
        </div>
  
        <div class="form-group">
          <label for="email">Email: </label>
          <input type="text" name="email" class="form-control" value="{{ old('email', $user->email) }}">
      @error('email')
          <div class="alert alert-danger">{{ $message }}</div>
      @enderror
        </div>
  
        <div class="form-group">
          <label class="my-1 mr-2" for="status">Status: </label>
          <select class="custom-select my-1 mr-sm-2" id="status" name="status">
              <option value="admin">admin</option>
              <option value="staff">staff</option>
            </select>
        </div>
  
      <button type="submit" class="btn btn-primary">Submit</button>
    </form>

@endsection