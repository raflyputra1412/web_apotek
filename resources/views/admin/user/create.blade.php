@extends('admin.layout.master')
@section('judul')

Manage User

@endsection

@section('content')

<form action="/kelola_user" method="POST">
    @csrf
    <div class="form-group">
      <label for="nama_user">Nama User: </label>
      <input type="text" name="nama_user" class="form-control" value="{{ old('nama_user') }}">
    @error('nama_user')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    </div>

    <div class="form-group">
      <label for="password">Password: </label>
      <input type="password" name="password" class="form-control" value="{{ old('password') }}">
  @error('password')
      <div class="alert alert-danger">{{ $message }}</div>
  @enderror
    </div>

    <div class="form-group">
        <label for="alamat">Alamat: </label>
        <input type="text" name="alamat" class="form-control" value="{{ old('alamat') }}">
    @error('alamat')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
      </div>
      
      <div class="form-group">
        <label for="phone">Phone: </label>
        <input type="text" name="phone" class="form-control" value="{{ old('phone') }}">
    @error('phone')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
      </div>

      <div class="form-group">
        <label for="email">Email: </label>
        <input type="text" name="email" class="form-control" value="{{ old('email') }}">
    @error('email')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
      </div>

      <div class="form-group">
        <label class="my-1 mr-2" for="status">Status: </label>
        <select class="custom-select my-1 mr-sm-2" id="status" name="status">
            <option selected>Pilih Status...</option>
                <option value="admin">admin</option>
                <option value="staff">staff</option>
        </select>
      </div>

    <button type="submit" class="btn btn-primary">Submit</button>
  </form>

@endsection


@push('scripts')
  <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
@endpush