<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="../../index3.html" class="brand-link">
      <img src="{{asset('template/dist/img/AdminLTELogo.png')}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light">Apotek SanberHealth</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{asset('template/dist/img/user2-160x160.jpg')}}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">{{Auth::user()->nama_user}}</a>
        </div>
      </div>

      <!-- SidebarSearch Form -->
      <div class="form-inline">
        <div class="input-group" data-widget="sidebar-search">
          <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
          <div class="input-group-append">
            <button class="btn btn-sidebar">
              <i class="fas fa-search fa-fw"></i>
            </button>
          </div>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
        <li class="nav-item">
            <a href="../widgets.html" class="nav-link">
              <i class="nav-icon fas fa-home"></i>
              <p>
                Home
              </p>
            </a>
        </li>
        <li class="nav-item">
            <a href="/kelola_user" class="nav-link">
              <i class="nav-icon fas fa-user"></i>
              <p>
                Manage User
              </p>
            </a>
        </li>
            <li class="nav-item">
            <a href="" class="nav-link">
              <i class="nav-icon fa fa-flask"></i>
              <p>
                Obat
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="/jenisObat" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Jenis Obat</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/obat" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Data Obat</p>
                </a>
              </li>
            </ul>
            </li>
            <li class="nav-item">
              <a href="/dokter" class="nav-link">
                <i class="nav-icon fa fa-user-md"></i>
                <p>
                  Dokter
                </p>
              </a>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fa fa-file"></i>
              <p>
                Content
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="/jeniscontent" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Jenis Content</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/content" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Content</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item mt-4 ml-3">
            <a href="/logout" class="nav-link">
              <p class="text-red">
                Logout
              </p>
            </a>
        </li>
          
         
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>