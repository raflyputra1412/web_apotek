@extends('admin.layout.master')
@section('judul')
Halaman Jenis Content
@endsection
@section('content')
<a href="/jeniscontent/create" class="btn btn-primary btn-sm my-3">Input Jenis Content</a>
<table id="jeniscontent" class="table table-bordered table-striped">
    <thead>
    <tr>
      <th>No</th>
      <th>Nama Jenis Content</th>
      <th>Action</th>
    </tr>
    </thead>
    <tbody>
        @forelse ($jeniscontent as $key =>$item)
        <tr>
            <td>{{$key+1}}</td>
            <td>{{$item->nama_jenis}}</td>
            <td>
                <form action="/jeniscontent/{{$item->id}}" method="POST">
                    @csrf
                    @method('DELETE')
                        <a href="/jeniscontent/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                        <input type="submit" value="delete" class="btn btn-danger btn-sm">
                  </form>
            </td>
            </tr>
        @empty
            <tr>
                <td>Tidak ada Content</td>
            </tr>
        @endforelse
        
   </tbody>
</table>
@endsection
