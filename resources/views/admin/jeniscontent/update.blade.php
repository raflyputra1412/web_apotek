@extends('admin.layout.master')
@section('judul')
  Halaman Jenis Content
@endsection
@section('content')
<form action="/jeniscontent/{{$jeniscontent->id}}" method="POST">
  @csrf
  @method('PUT')
  <div class="form-group">
    <label>Jenis Content</label>
    <input type="text" name="nama" value="{{old('nama',$jeniscontent->nama_jenis)}}" class="form-control" >
  </div>
  @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
  
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection