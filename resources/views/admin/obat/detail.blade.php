@extends('admin.layout.master')
@section('judul')
    Halaman Detail Obat
@endsection
@section('content')
<a href="/obat" class="btn btn-primary btn-sm my-3">Kembali</a>
<div class="card">
    <h2>{{$obat->nama_obat}}</h2>
    <img src={{asset('image/obat/'.$obat->gambar)}}>
    <div class="card-body">
        <h5>Jenis Obat : {{$obat->jenisObat->nama_jenis}}</h5>      
        <h5>Harga : {{$obat->harga}}</h5>      
        <h5>Ketersediaan : {{$obat->ketersediaan}}</h5>      
        <p>Indikasi :{{$obat->indikasi}}</p>      
    </div>
  </div>
@endsection