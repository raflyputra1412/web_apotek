@extends('admin.layout.master')
@section('judul')

Daftar Obat

@endsection

@push('style')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.12.1/datatables.min.css"/>

@endpush
@section('content')
<a href="/obat/create" class="btn btn-primary btn-sm my-3">Input Obat</a>
<table id="dataObat" class="table table-bordered table-striped">
    <thead>
    <tr>
      <th>No</th>
      <th>Nama Obat</th>
      <th>Harga</th>
      <th>Ketersediaan</th>
      <th>Action</th>
    </tr>
    </thead>
    <tbody>
        @forelse ($obat as $key =>$item)
        <tr>
            <td>{{$key+1}}</td>
            <td>{{$item->nama_obat}}</td>
            <td>{{$item->harga}}</td>
            <td>{{$item->harga}}</td>
            <td>
                <form action="/obat/{{$item->id}}" method="POST">
                    @csrf
                    @method('DELETE')
                        <a href="/obat/{{$item->id}}" class="btn btn-info btn-sm">Detail Obat</a>
                        <a href="/obat/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit Obat</a>
                        <input type="submit" value="delete" class="btn btn-danger btn-sm">
                  </form>
            </td>
            </tr>
        @empty
            <tr>
                <td>Tidak ada data Obat</td>
            </tr>
        @endforelse
        
   </tbody>
</table>
@endsection

@push('scripts')
    <script src="{{asset('template/plugins/datatables/jquery.dataTables.js')}}"></script>
    <script src="{{asset('template/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
    <script>
    $(function () {
        $("#dataObat").DataTable();
    });
    </script>
@endpush