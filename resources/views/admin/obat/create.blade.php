@extends('admin.layout.master')
@section('judul')
Halaman Tambah Obat
@endsection
@section('content')
<form action="/obat" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
      <label>Nama Obat</label>
      <input type="text" name="namaObat" value="{{old('namaObat')}}" class="form-control">
    </div>
    @error('namaObat')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>Indikasi</label>
      <textarea name="indikasi" cols="30" rows="10" class="form-control">{{old('indikasi')}}</textarea>
    </div>
    @error('indikasi')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
      <div class="form-group">
        <label>Gambar</label>
        <input type="file" name="gambar" value="{{old('gambar')}}" class="form-control" id="">    
    </div>
    @error('gambar')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Jenis Obat</label>
      <select name="jenisObat" id="" class="form-control">
          <option value="">--Pilih Jenis Obat--</option>
          @forelse ($jenisObat as $item)
            <option value="{{$item->id}}">{{$item->nama_jenis}}</option>              
          @empty
              <option value="">Tidak Ada Jenis Content</option>
          @endforelse
      </select>
    </div>
    @error('jenisObat')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>Harga Obat</label>
      <input type="number" name="harga" value="{{old('harga')}}" class="form-control">
    </div>
    @error('harga')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>Ketersediaan</label>
    <select name="ketersediaan" id="" class="form-control">
        {{-- <option value="">--Pilih Jenis Obat--</option> --}}
        <option value="">-- Ketersediaan Obat --</option>
        <option value="tersedia">Tersedia</option>
        <option value="habis">Habis</option>
    </select>
    </div>
    @error('ketersediaan')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
  
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection