@extends('admin.layout.master')
@section('judul')
Halaman Detail Content
@endsection
@section('content')

        <div class="card">
            <img class="card-img-top" src="{{asset('image/content/'.$content->gambar)}}" style="width: 100vh; height: 400px" alt="Card image cap">
            <div class="card-body">
                <h2>{{$content->judul}}</h2>
                {{-- tanpa limit karakter --}}
                <p class="card-text">{{($content->ringkasan)}}</p>
                {{-- dengan limit karakter --}}
                {{-- <p class="card-text">{{ Str::limit($item->ringkasan, 20) }}</p> --}}
                <a href="/content" class="btn btn-primary">Kembali</a>
            </div>
    </div>

@endsection