@extends('admin.layout.master')
@section('judul')
Halaman Content
@endsection
@section('content')

<a href="/content/create" class="btn btn-primary btn-sm my-3"> Tambah Content</a>

<div class="row">
    @forelse ($content as $item)
        <div class="col-4">
            <div class="card">
                <img class="card-img-top" src="{{asset('image/content/'.$item->gambar)}}" alt="Card image cap">
                <div class="card-body">
                    <h5>{{$item->judul}}</h5>
                    {{-- tanpa limit karakter --}}
                    {{-- <p class="card-text">{{($item->ringkasan)}}</p> --}}
                    {{-- dengan limit karakter --}}
                    <p class="card-text">{{ Str::limit($item->ringkasan, 20) }}</p>
                    <form action="/content/{{$item->id}}" method="POST">
                        @csrf
                        @method('DELETE')
                        <a href="/content/{{$item->id}}" class="btn btn-primary">Detail</a>
                        <a href="/content/{{$item->id}}/edit" class="btn btn-warning">Edit</a>
                        <input type="submit" value="Delete"  class="btn btn-danger">
                    </form>
                </div>
              </div>
        </div>
    @empty
        <h1> Tidak Ada Content</h1>
    @endforelse

</div>

@endsection