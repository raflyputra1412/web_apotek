@extends('admin.layout.master')
@section('judul')
Halaman Update Content
@endsection
@section('content')
<form action="/content/{{$content->id}}" method="POST" enctype="multipart/form-data">
    @csrf
    @method('put')
    <div class="form-group">
      <label>Judul</label>
      <input type="text" name="judul" value="{{old('judul', $content->judul)}}" class="form-control">
    </div>
    @error('name')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>Ringkasan</label>
      <textarea name="ringkasan" cols="30" rows="10" class="form-control">{{old('ringkasan', $content->ringkasan)}}</textarea>
    </div>
    <div class="form-group">
        <label>Deskripsi</label>
        <textarea class="ckeditor" name="deskripsi" cols="30" rows="10" class="form-control">{{old('deskripsi' , $content->deskripsi)}}</textarea>
      </div>
      <div class="form-group">
        <label>Gambar</label>
        <input type="file" name="gambar" class="form-control" id="">    
    </div>
    <div class="form-group">
        <label>Jenis Content</label>
      <select name="jenis_content_id" id="" class="form-control">
          <option value="">--Pilih Jenis Content--</option>
          @forelse ($jenis_content as $item)
            @if ($item->id === $content->jenis_content_id)
            <option value="{{$item->id}}" selected>{{$item->nama_jenis}}</option>
                
            @else
            <option value="{{$item->id}}">{{$item->nama_jenis}}</option>
          @endif         
          @empty
              <option value="">Tidak Ada Jenis Content</option>
          @endforelse
      </select>
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection

@push('scripts')
<script src="//cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>

<script type="text/javascript">

    $(document).ready(function() {

       $('.ckeditor').ckeditor();

    });

</script>
@endpush