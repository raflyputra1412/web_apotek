@extends('admin.layout.master')
@section('judul')
Halaman Tambah Content
@endsection
@section('content')
<form action="/content" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
      <label>Judul</label>
      <input type="text" name="judul" value="{{old('judul')}}" class="form-control">
    </div>
    @error('name')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>Ringkasan</label>
      <textarea name="ringkasan" cols="30" rows="10" class="form-control">{{old('ringkasan')}}</textarea>
    </div>
    <div class="form-group">
        <label>Deskripsi</label>
        <textarea class="ckeditor" name="deskripsi" cols="30" rows="10" class="form-control">{{old('deskripsi')}}</textarea>
      </div>
      <div class="form-group">
        <label>Gambar</label>
        <input type="file" name="gambar" class="form-control" id="">    
    </div>
    <div class="form-group">
        <label>Jenis Content</label>
      <select name="jenis_content_id" id="" class="form-control">
          <option value="">--Pilih Jenis Content--</option>
          @forelse ($jenis_content as $item)
            <option value="{{$item->id}}">{{$item->nama_jenis}}</option>              
          @empty
              <option value="">Tidak Ada Jenis Content</option>
          @endforelse
      </select>
      </div>
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection


@push('scripts')
<script src="//cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>

<script type="text/javascript">

    $(document).ready(function() {

       $('.ckeditor').ckeditor();

    });

</script>
@endpush