@extends('admin.layout.master')
@section('judul')

Add Dokter

@endsection

@section('content')

<form action="/dokter" method="POST">
    @csrf
    <div class="form-group">
      <label for="nama_dokter">Nama Dokter: </label>
      <input type="text" name="nama_dokter" class="form-control" value="{{ old('nama_dokter') }}">
    @error('nama_dokter')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    </div>

    <div class="form-group">
      <label for="alamat">Alamat: </label>
      <input type="text" name="alamat" class="form-control" value="{{ old('alamat') }}">
  @error('alamat')
      <div class="alert alert-danger">{{ $message }}</div>
  @enderror
    </div>

    <div class="form-group">
        <label for="phone">Phone: </label>
        <input type="text" name="phone" class="form-control" value="{{ old('phone') }}">
    @error('phone')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
      </div>
      
      <div class="form-group">
        <label for="jam_praktik">Jam Praktik: </label>
        <input type="text" name="jam_praktik" class="form-control" value="{{ old('jam_praktik') }}">
    @error('jam_praktik')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
      </div>

      <div class="form-group">
        <label for="user_id">Input By: </label>
        <select name="user_id" id="" class="form-control">
          <option value="">Pilih User</option>
            <option value="{{ $user->id }}">{{ $user->nama_user}}</option>
        </select>
    @error('user_id')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
      </div>

    <button type="submit" class="btn btn-primary">Submit</button>
  </form>

@endsection
