@extends('admin.layout.master')
@section('judul')
    Edit User
@endsection
@section('content')

<form action="/dokter/{{ $dokter->id }}" method="POST">
    @csrf
    @method('PUT')

    <div class="form-group">
        <label for="nama_user">Nama User: </label>
        <input type="text" name="nama_dokter" class="form-control" value="{{ old('nama_dokter', $dokter->nama_dokter) }}">
      @error('nama_dokter')
          <div class="alert alert-danger">{{ $message }}</div>
      @enderror
      </div>
  
      <div class="form-group">
          <label for="alamat">Alamat: </label>
          <input type="text" name="alamat" class="form-control" value="{{ old('alamat', $dokter->alamat) }}">
      @error('alamat')
          <div class="alert alert-danger">{{ $message }}</div>
      @enderror
        </div>
        
        <div class="form-group">
          <label for="phone">Phone: </label>
          <input type="text" name="phone" class="form-control" value="{{ old('phone', $dokter->phone) }}">
      @error('phone')
          <div class="alert alert-danger">{{ $message }}</div>
      @enderror
        </div>
  
        <div class="form-group">
          <label for="email">Email: </label>
          <input type="text" name="jam_praktik" class="form-control" value="{{ old('jam_praktik', $dokter->jam_praktik) }}">
      @error('email')
          <div class="alert alert-danger">{{ $message }}</div>
      @enderror
        </div>
  
        <div class="form-group">
          <label for="user_id">Input By: </label>
          <select name="user_id" id="" class="form-control">
            <option value="">Pilih User</option>
              <option value="{{ $user->id }}">{{ $user->nama_user}}</option>
          </select>
      @error('user_id')
          <div class="alert alert-danger">{{ $message }}</div>
      @enderror
        </div>
  
      <button type="submit" class="btn btn-primary">Submit</button>
    </form>

@endsection