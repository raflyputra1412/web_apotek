@extends('admin.layout.master')
@section('judul')

Manage Dokter

@endsection

@push('style')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.12.1/datatables.min.css"/>
@endpush

@section('content')
@if (session()->has('success'))
<div class="alert alert-primary" role="alert">
    <span>{{ session('success') }}</span>
</div>
@endif

@if(session()->has('error'))
<div class="alert alert-danger" role="alert">
    <span>{{ session('error') }}</span>
</div>
@endif

<a href="/dokter/create" class="btn btn-primary mb-3">Add New Dokter</a>

<table id="dataDokter" class="table table-bordered table-striped">
    <thead>
      <tr>
        <th scope="col">ID</th>
        <th scope="col">Nama Dokter</th>
        <th scope="col">Alamat</th>
        <th scope="col">Phone</th>
        <th scope="col">Jam Praktik</th>
        <th scope="col">Input By</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
      @forelse ($dokter as $key => $item)
      <tr>
        <td> {{ $item->id }} </td>
        <td> {{ $item->nama_dokter }} </td>
        <td> {{ $item->alamat }} </td>
        <td> {{ $item->phone }} </td>
        <td> {{ $item->jam_praktik }} </td>
        <td> {{ $item->user->nama_user }} </td>
        <td>
          <form action="/dokter/{{ $item->id }}" method="POST">
            @csrf
            @method('delete')
            <a href="/dokter/{{ $item->id }}/edit" class="btn btn-warning btn-sm"> EDIT </a>
            <input type="submit" class="btn btn-danger btn-sm" value="DELETE">
          </form>
        </td>
      </tr>

      @empty

      <tr>
        <td>No Data</td>
      </tr>

      @endforelse
    </tbody>
  </table>


@endsection

@push('scripts')
    <script src="{{asset('template/plugins/datatables/jquery.dataTables.js')}}"></script>
    <script src="{{asset('template/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
    <script>
    $(function () {
        $("#dataDokter").DataTable();
    });
    </script>
@endpush