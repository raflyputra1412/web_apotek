## Final Project

## Kelompok 9

- Rizki Muhamad Aris (@Rizky_Aris)
- Muhammad Rafly Hersa Putra (@Rafly Putra)
- Ahmad Fauzi (Fauzi263)

## Tema Project

Sistem Informasi Apotek

## ERD

<p align="center"><img src="public/image/ERD.jpeg" /></p>

## Link Video

Link Demo Aplikasi : https://www.youtube.com/watch?v=6TaSUzv2kwk

Link Deploy : http://sanberapotek.webapotekonline.sanbercodeapp.com