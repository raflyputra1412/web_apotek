<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
class auth_check
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $roles)
    {

        if (!Auth::check()) {
            return redirect('/');
        }
        $user = Auth::user();

        if($user->status == $roles){
            return $next($request);
        }

        return redirect('/');
    }

}
