<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\User;

class AuthController extends Controller
{
    public function index(){
        return view('auth.index');
    }

    public function store(Request $request){
        $rules = $request->validate([
            'nama_user' => 'required|max:255',
            'password' => 'required|min:5',
        ]);

        if (Auth::attempt($rules)) {
            $user = Auth::user();
            if($user->status == 'admin'){
                $request->session()->regenerate();
                return redirect()->intended('/dashboard_admin');
            }
            elseif ($user->status == 'staff') {
                $request->session()->regenerate();
                return redirect()->intended('/dashboard_staff');
            }
        }
        
        return redirect('/')->with('error','A problem has been occurred while submitting your data.');
    }

    public function logout(Request $request){
        $request->session()->flush();
        Auth::logout();

        return redirect('/');
    }
}