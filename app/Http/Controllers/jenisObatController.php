<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\JenisObat;

class jenisObatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $jenisObat = JenisObat::all();
        return view('admin.jenisObat.index',compact('jenisObat'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.jenisObat.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama'=>'required',
        ]);

        $jenisObat = new JenisObat;
 
        $jenisObat->nama_jenis = $request->nama;
 
        $jenisObat->save();

        return redirect ('/jenisObat');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $jenisObat = JenisObat::find($id);

        return view('admin.jenisObat.update',compact('jenisObat'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama'=>'required',
        ]);
        $jenisObat = JenisObat::find($id);
        $jenisObat->nama_jenis = $request->nama;

        $jenisObat->save();

        return redirect ('/jenisObat');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $jenisObat = JenisObat::find($id);
        $jenisObat->delete();
        return redirect ('/jenisObat');

    }
}
