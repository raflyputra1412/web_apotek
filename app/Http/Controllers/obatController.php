<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use App\JenisObat;
use App\User;
use App\Obat;
use File;

class obatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $obat = obat::all();
        return view('admin.obat.index',compact('obat'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        $jenisObat = JenisObat::all();
        return view('admin.obat.create', compact('jenisObat'));

        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'namaObat'=>'required',
            'indikasi'=>'required',
            'jenisObat'=>'required',
            'gambar' => 'required|mimes:jpg,jpeg,png|max:2048',
            'harga'=>'required',
            'ketersediaan'=>'required',
        ]);
  
        $namaGambar = time().'.'.$request->gambar->extension();  
   
        $request->gambar->move(public_path('image/obat'), $namaGambar); 
        
        $obat = new obat;
 
        $obat->nama_obat = $request->namaObat;
        $obat->gambar = $namaGambar;
        $obat->ketersediaan = $request->ketersediaan;
        $obat->indikasi = $request->indikasi;
        $obat->harga = $request->harga;
        $obat->jenis_obat_id = $request->jenisObat;
        $obat->user_id = Auth::id();
 
        $obat->save();

        return view ('/admin/obat/index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $obat = obat::find($id);
        return view('admin.obat.detail',compact('obat'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $jenisObat = JenisObat::all();
        $obat = Obat::find($id);
        return view('admin.obat.update',compact('obat','jenisObat'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'namaObat'=>'required',
            'indikasi'=>'required',
            'jenisObat'=>'required',
            'harga'=>'required',
            'ketersediaan'=>'required',
        ]);

        $obat=Obat::find($id);
        if ($request->has('gambar')) {
            $path="image/obat/";         
            file::delete($path . $obat->gambar);
            $namaGambar = time().'.'.$request->gambar->extension();
            $request->gambar->move(public_path('image/obat'), $namaGambar); 

            $obat->gambar=$namaGambar;
            $obat->save();

        }

        $obat->nama_obat = $request->namaObat;
        $obat->ketersediaan = $request->ketersediaan;
        $obat->indikasi = $request->indikasi;
        $obat->harga = $request->harga;
        $obat->jenis_obat_id = $request->jenisObat;
        $obat->user_id = Auth::id();
 
        $obat->save();

        return redirect ('/obat');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $obat = Obat::find($id);
        
        $path="image/obat/";
        file::delete($path . $obat->poster);
        
        $obat->delete();

        return redirect ('/obat');
    }
}
