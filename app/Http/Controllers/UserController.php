<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;


class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = User::all();
        return view('admin.user.index', compact('user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama_user' => 'required|max:100',
            'password' => 'required|max:5', 
            'alamat' => 'required|max:100',
            'phone' => 'required',
            'email' => 'required|max:70',
            'status' => 'required',
        ]);
        $request['password'] = Hash::make($request['password']);

        $user = new User;
        $user->nama_user = $request->input('nama_user');
        $user->password = $request->input('password');
        $user->alamat = $request->input('alamat');
        $user->phone = $request->input('phone');
        $user->email = $request->input('email');
        $user->status = $request->input('status');
        $user->save();

        return redirect('/kelola_user')->with('success', 'User has been added successfully!');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        return view('admin.user.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //rules
        $request->validate([
            'nama_user' => 'required|max:100', 
            'alamat' => 'required|max:100',
            'phone' => 'required|max:13',
            'email' => 'required|max:70',
            'status' => 'required',
        ]);

        $user = User::find($id);

        $user->nama_user = $request->nama_user;
        $user->alamat = $request->alamat;
        $user->phone = $request->phone;
        $user->email = $request->email;
        $user->status = $request->status; 
        $user->save();

        return redirect('/kelola_user')->with('success', 'User has been updated successfully!');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();

        return redirect('/kelola_user')->with('success', 'User has been deleted!');

    }
}
