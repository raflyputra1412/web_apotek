<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\JenisContent;

class JenisContentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $jeniscontent = JenisContent::all();
        return view('admin.jeniscontent.index',compact('jeniscontent'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.jeniscontent.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama'=>'required',
        ]);

        $jeniscontent = new Jeniscontent;
 
        $jeniscontent->nama_jenis = $request->nama;
 
        $jeniscontent->save();

        return redirect ('/jeniscontent');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $jeniscontent = JenisContent::find($id);

        return view('admin.jeniscontent.update',compact('jeniscontent'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama'=>'required',
        ]);
        $jeniscontent = JenisContent::find($id);
        $jeniscontent->nama_jenis = $request->nama;

        $jeniscontent->save();

        return redirect ('/jeniscontent');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $jeniscontent = JenisContent::find($id);
        $jeniscontent->delete();
        return redirect ('/jeniscontent');
    }
}
