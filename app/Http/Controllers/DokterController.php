<?php

namespace App\Http\Controllers;

use App\Dokter;
use App\User;
use Illuminate\Support\Facades\Auth;

use Illuminate\Http\Request;

class DokterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dokter = Dokter::all();
        return view('admin.dokter.index', compact('dokter'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $dokter = Dokter::get();
        $user = Auth::user();
        return view('admin.dokter.create', compact('dokter', 'user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama_dokter' => 'required',
            'alamat' => 'required',
            'phone' => 'required',
            'jam_praktik' => 'required',
            'user_id' => 'required',
        ]);

        $dokter = new Dokter;

        $dokter->nama_dokter = $request->input('nama_dokter');
        $dokter->alamat = $request->input('alamat');
        $dokter->phone = $request->input('phone');
        $dokter->jam_praktik = $request->input('jam_praktik');
        $dokter->user_id = $request->input('user_id');
        $dokter->save();
        return redirect('/dokter')->with('success', 'New Dokter has been added successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Dokter  $dokter
     * @return \Illuminate\Http\Response
     */
    public function show(Dokter $dokter)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Dokter  $dokter
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $dokter = Dokter::find($id);
        $user = Auth::user();
        return view('admin.dokter.edit', compact('dokter', 'user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Dokter  $dokter
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama_dokter' => 'required',
            'alamat' => 'required',
            'phone' => 'required',
            'jam_praktik' => 'required',
            'user_id' => 'required',
        ]);

        $dokter = Dokter::find($id);

        $dokter->nama_dokter = $request->nama_dokter;
        $dokter->alamat = $request->alamat;
        $dokter->phone = $request->phone;
        $dokter->jam_praktik = $request->jam_praktik;
        $dokter->user_id = $request->user_id;
        $dokter->save();
        return redirect('/dokter')->with('success', 'Dokter has been updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Dokter  $dokter
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $dokter = Dokter::find($id);
        $dokter->delete();
        
        return redirect('/dokter')->with('success', 'Dokter has been deleted');
    }
}
