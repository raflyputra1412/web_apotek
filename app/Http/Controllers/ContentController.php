<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use App\JenisContent;
use App\User;
use App\Content;
use File;

class ContentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $content = Content::all();
        return view('admin.content.index', compact('content'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $jenis_content = JenisContent::all();
        return view('admin.content.create', compact('jenis_content'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $request->validate([
            'judul' => 'required',
            'ringkasan' => 'required',
            'deskripsi' => 'required',
            'gambar' => 'required|mimes:jpg,png,jpeg|max:2048',
            'jenis_content_id' => 'required'
        ]);
    
        $NamaGambar = time().'.'.$request->gambar->extension();  
   
        $request->gambar->move(public_path('image/content'), $NamaGambar);
   
        $content = new Content;
 
        $content->judul = $request->judul;
        $content->ringkasan = $request->ringkasan;
        $content->deskripsi = $request->deskripsi;
        $content->gambar = $NamaGambar;
        $content->user_id = Auth::id();
        $content->jenis_content_id = $request->jenis_content_id;
 
        $content->save();

        return redirect('/content');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $content = Content::find($id);
        return view('admin.content.detail', compact('content'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $jenis_content = JenisContent::all();
        $content = Content::find($id);
        return view('admin.content.update', compact('content', 'jenis_content'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'judul' => 'required',
            'ringkasan' => 'required',
            'deskripsi' => 'required',
            'gambar' => 'mimes:jpg,png,jpeg|max:2048',
            'jenis_content_id' => 'required'
        ]);

        $content = Content::find($id);
        if ($request->has('gambar')) {
            $path = "image/content/";
            File::delete($path . $content->gambar);


            $NamaGambar = time().'.'.$request->gambar->extension();  

            $request->gambar->move(public_path('image/content'), $NamaGambar);

            $content->gambar = $NamaGambar;

            $content->save();
        }
        $content->judul = $request->judul;
        $content->ringkasan = $request->ringkasan;
        $content->deskripsi = $request->deskripsi;
        $content->user_id = Auth::id();
        $content->jenis_content_id = $request->jenis_content_id;

        $content->save();

        return redirect("/content");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $content = Content::find($id);

        $path = "image/content";
        File::delete($path . $content->gambar);

        $content->delete();

        return redirect("/content");
    }
}
