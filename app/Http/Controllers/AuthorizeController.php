<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;

class AuthorizeController extends Controller
{
    public function index(){
        return view('authorize.index');;
    }

    public function store(Request $request){
        // dd($request);
        $request->validate([
            'nama_user' => 'required|max:100',
            'password' => 'required|max:5', 
            'alamat' => 'required|max:100',
            'phone' => 'required',
            'email' => 'required|max:70',
            'status' => 'required',
        ]);
        $request['password'] = Hash::make($request['password']);

        $user = new User;
        $user->nama_user = $request->input('nama_user');
        $user->password = $request->input('password');
        $user->alamat = $request->input('alamat');
        $user->phone = $request->input('phone');
        $user->email = $request->input('email');
        $user->status = $request->input('status');
        $user->save();

        return redirect('/')->with('success', 'Registration Success!');
    }

}
