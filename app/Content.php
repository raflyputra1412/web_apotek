<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Content extends Model
{
    protected $table = 'content';

    protected $fillable = ['judul','ringkasan','deskripsi','gambar','user_id','jenis_content_id'];

    public function JenisContent(){
        return $this->belongsTo('App\JenisContent','jenis_content_id');
    }
   
}
