<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JenisContent extends Model
{
    protected $table = 'jenis_content';

    protected $fillable = ['nama_jenis'];
}
