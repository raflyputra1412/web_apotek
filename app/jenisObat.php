<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class jenisObat extends Model
{
    protected $table = 'jenis_obat';

    protected $fillable = ['nama_jenis'];
}
