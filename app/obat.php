<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class obat extends Model
{
    protected $table = 'obat';

    protected $fillable = ['nama_obat','gambar','ketersediaan','indikasi','harga','jenis_obat_id','user_id'];

    public function jenisObat(){
        return $this->belongsTo('App\JenisObat','jenis_obat_id');
    }
}
